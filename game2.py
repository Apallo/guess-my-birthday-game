from random import randint
for i in range(5):
    year = randint(1924 , 2004)
    month = randint(1 , 12)
    print("Guess #", i + 1,"Were you born in", month, "/", year, "?")
    print("Yes or No? ")
    guess = input().lower()
    if guess == "yes":
        print("I knew it!")
        exit()
    elif guess == "no" and i < 4:
        print("Dang! Let me try again!")
    else:
        print("Forget it! I have better things to do!")
        exit()